package pawel.config;

import liquibase.integration.spring.SpringLiquibase;
import org.springframework.context.annotation.*;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import javax.annotation.Resource;
import javax.sql.DataSource;

@Configuration
@Import(DataSourceConfig.class)
@ComponentScan(basePackages = {"pawel.dao", "pawel.config"})
public class AppConfiguration {

    @Resource(name = "postgres")
    private DataSource dataSource;

    @Bean
    @DependsOn("postgres")
    public SpringLiquibase liquibase() {
        SpringLiquibase liquibase = new SpringLiquibase();
        liquibase.setChangeLog("classpath:db/liqui/initTable.xml");
        liquibase.setDataSource(dataSource);
        return liquibase;
    }

    @Bean
    @DependsOn("postgres")
    public JdbcTemplate jdbcTemplate() {
        return new JdbcTemplate(dataSource);
    }

    @Bean
    @DependsOn("postgres")
    public NamedParameterJdbcTemplate namedParameterJdbcTemplate() {
        return new NamedParameterJdbcTemplate(dataSource);
    }

}
