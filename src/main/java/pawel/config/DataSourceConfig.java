package pawel.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

@Configuration
@PropertySource("classpath:db/database.properties")
public class DataSourceConfig {

    @Value("${driver}")
    private String driverClass;
    @Value("${url}")
    private String url;
    @Value("${user}")
    private String username;
    @Value("${password}")
    private String password;

    @Bean(name = "postgres")
    public DataSource dataSource() {
        DriverManagerDataSource driverDataSource = new DriverManagerDataSource();
        driverDataSource.setDriverClassName(driverClass);
        driverDataSource.setUrl(url);
        driverDataSource.setUsername(username);
        driverDataSource.setPassword(password);
        return driverDataSource;
    }
}
