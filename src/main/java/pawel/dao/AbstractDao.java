package pawel.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

class AbstractDao {

    private String URL = "jdbc:postgresql://localhost:5432/springjdbc";
    private String USER = "postgres";
    private String PASS = "postgres";
    private Connection connection;

    Connection getConnection() {
        try {
            connection = DriverManager.getConnection(URL, USER, PASS);
        } catch (SQLException ex) {
            System.out.printf("not possible establish connection, because %s, errorCode: %s",
                    ex.getMessage(), ex.getErrorCode());
        }
        return connection;
    }
}
