package pawel.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.util.StopWatch;
import pawel.model.Singer;

import java.util.Collections;
import java.util.List;

@Repository
public class JdbcTemplateDao implements SingerDao {

    //именованные параметры будут такие(firstName, lastName, birthDate), потому что трансформируются в BeanPropertySqlParameterSource
    private static final String INSERT_SINGER_QUERY = "insert into singer (first_name, last_name, birth_date) " +
            "values (:firstName, :lastName, :birthDate)";
    private static final String DELETE_SINGER_QUERY = "delete from singer where guid=:guid";
    private static final String UPDATE_SINGER_QUERY = "update singer set first_name=:firstName, " +
            "last_name=:lastName, birth_date=:birthDate where guid = :guid";

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public List<Singer> findAll() {
        StopWatch stopWatch = new StopWatch("findAll");
        stopWatch.start();
        List<Singer> result = jdbcTemplate.query(Singer.FIND_ALL_SINGERS_QUERY, (resultSet, rowNum) -> {
            Singer singer = new Singer();
            singer.setGuid(resultSet.getString(Singer.GUID));
            singer.setFirstName(resultSet.getString(Singer.FIRST_NAME));
            singer.setLastName(resultSet.getString(Singer.LAST_NAME));
            singer.setBirthDate(resultSet.getDate(Singer.BIRTH_DATE));
            return singer;
        });
        stopWatch.stop();
        System.out.println(stopWatch.prettyPrint());
        return result;
    }

    @Override
    public String insert(Singer singer) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        SqlParameterSource namedParameters = new BeanPropertySqlParameterSource(singer);
        StopWatch stopWatch = new StopWatch("insert");
        stopWatch.start();
        namedParameterJdbcTemplate.update(INSERT_SINGER_QUERY, namedParameters, keyHolder);
        stopWatch.stop();
        System.out.println(stopWatch.prettyPrint());
        return (String)keyHolder.getKeys().get(Singer.GUID);
    }

    @Override
    public void update(Singer singer) {
        SqlParameterSource namedParameters = new BeanPropertySqlParameterSource(singer);
        StopWatch stopWatch = new StopWatch("update");
        stopWatch.start();
        namedParameterJdbcTemplate.update(UPDATE_SINGER_QUERY, namedParameters);
        stopWatch.stop();
        System.out.println(stopWatch.prettyPrint());
    }

    @Override
    public void delete(String guid) {
        StopWatch stopWatch = new StopWatch("delete");
        stopWatch.start();
        namedParameterJdbcTemplate.update(DELETE_SINGER_QUERY, Collections.singletonMap(Singer.GUID, guid));
        stopWatch.stop();
        System.out.println(stopWatch.prettyPrint());
    }
}
