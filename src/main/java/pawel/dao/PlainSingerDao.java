package pawel.dao;

import org.springframework.util.StopWatch;
import pawel.model.Singer;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class PlainSingerDao extends AbstractDao implements SingerDao {

    private static final String INSERT_SINGER_QUERY = "insert into singer (first_name, last_name, birth_date) " +
            "values (?, ?, ?)";
    private static final String DELETE_SINGER_QUERY = "delete from singer where guid =?";
    private static final String UPDATE_SINGER_QUERY = "update singer set first_name=?, last_name=?," +
            " birth_date=? where guid =?";

    @Override
    public List<Singer> findAll() {
        StopWatch stopWatch = new StopWatch("findAll");
        stopWatch.start();
        List<Singer> singers = new ArrayList<>();
        try(Connection connection = getConnection()) {
            PreparedStatement statement = connection.prepareStatement(Singer.FIND_ALL_SINGERS_QUERY);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Singer singer = new Singer();
                singer.setGuid(resultSet.getString(Singer.GUID));
                singer.setFirstName(resultSet.getString(Singer.FIRST_NAME));
                singer.setLastName(resultSet.getString(Singer.LAST_NAME));
                singer.setBirthDate(resultSet.getDate(Singer.BIRTH_DATE));
                singers.add(singer);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } finally {
            stopWatch.stop();
            System.out.println(stopWatch.prettyPrint());
        }
        return singers;
    }

    @Override
    public String insert(Singer singer) {
        StopWatch stopWatch = new StopWatch("insert");
        stopWatch.start();
        try(Connection connection = getConnection()) {
            PreparedStatement statement = connection.prepareStatement(INSERT_SINGER_QUERY, Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, singer.getFirstName());
            statement.setString(2, singer.getLastName());
            statement.setDate(3, singer.getBirthDate());
            statement.executeUpdate();
            ResultSet generatedKeys = statement.getGeneratedKeys();
            if (generatedKeys.next()) {
                singer.setGuid(generatedKeys.getString(1));
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } finally {
            stopWatch.stop();
            System.out.println(stopWatch.prettyPrint());
        }
        return singer.getGuid();
    }

    @Override
    public void delete(String guid) {
        StopWatch stopWatch = new StopWatch("delete");
        stopWatch.start();
        try (Connection connection = getConnection()) {
            PreparedStatement statement = connection.prepareStatement(DELETE_SINGER_QUERY);
            statement.setString(1, guid);
            statement.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } finally {
            stopWatch.stop();
            System.out.println(stopWatch.prettyPrint());
        }
    }

    @Override
    public void update(Singer singer) {
        StopWatch stopWatch = new StopWatch("update");
        stopWatch.start();
        try (Connection connection = getConnection()) {
            PreparedStatement statement = connection.prepareStatement(UPDATE_SINGER_QUERY);
            statement.setString(1, singer.getFirstName());
            statement.setString(2, singer.getLastName());
            statement.setDate(3, singer.getBirthDate());
            statement.setString(4, singer.getGuid());
            statement.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } finally {
            stopWatch.stop();
            System.out.println(stopWatch.prettyPrint());
        }
    }
}
