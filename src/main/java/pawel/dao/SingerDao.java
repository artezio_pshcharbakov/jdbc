package pawel.dao;

import pawel.model.Singer;

import java.util.List;

public interface SingerDao {



    List<Singer> findAll();
    String insert(Singer singer);
    void update(Singer singer);
    void delete(String guid);
}
