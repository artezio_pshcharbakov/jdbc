package pawel.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.util.StopWatch;
import pawel.dao.springClasses.InsertSinger;
import pawel.dao.springClasses.SelectAllSingers;
import pawel.dao.springClasses.UpdateSinger;
import pawel.model.Singer;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class SpringDaoClasses implements SingerDao {

    @Autowired
    private SelectAllSingers allSingers;
    @Autowired
    private UpdateSinger updateSinger;
    @Autowired
    private InsertSinger insertSinger;

    @Override
    public List<Singer> findAll() {
        StopWatch stopWatch = new StopWatch("findAll");
        stopWatch.start();
        List<Singer> singers = allSingers.execute();
        stopWatch.stop();
        System.out.println(stopWatch.prettyPrint());
        return singers;
    }

    @Override
    public String insert(Singer singer) {
        Map<String, Object> params = new HashMap<>();
        params.put(Singer.FIRST_NAME, singer.getFirstName());
        params.put(Singer.LAST_NAME, singer.getLastName());
        params.put(Singer.BIRTH_DATE, singer.getBirthDate());
        KeyHolder key = new GeneratedKeyHolder();
        StopWatch stopWatch = new StopWatch("insert");
        stopWatch.start();
        insertSinger.updateByNamedParam(params, key);
        stopWatch.stop();
        System.out.println(stopWatch.prettyPrint());
        return (String)key.getKeys().get(Singer.GUID);
    }

    @Override
    public void update(Singer singer) {
        Map<String, Object> params = new HashMap<>();
        params.put(Singer.FIRST_NAME, singer.getFirstName());
        params.put(Singer.LAST_NAME, singer.getLastName());
        params.put(Singer.BIRTH_DATE, singer.getBirthDate());
        params.put(Singer.GUID, singer.getGuid());
        StopWatch stopWatch = new StopWatch("update");
        stopWatch.start();
        updateSinger.updateByNamedParam(params);
        stopWatch.stop();
        System.out.println(stopWatch.prettyPrint());
    }

    @Override
    public void delete(String guid) {

    }
}
