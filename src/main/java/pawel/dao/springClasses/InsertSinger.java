package pawel.dao.springClasses;

import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.SqlUpdate;
import org.springframework.stereotype.Component;
import pawel.model.Singer;

import javax.sql.DataSource;
import java.sql.Types;

@Component
public class InsertSinger extends SqlUpdate {

    private static final String INSERT_SINGER_QUERY = "insert into singer (first_name, last_name, birth_date) " +
            "values (:first_name, :last_name, :birth_date)";

    public InsertSinger(DataSource ds) {
        super(ds, INSERT_SINGER_QUERY);
        super.declareParameter(new SqlParameter(Singer.FIRST_NAME, Types.VARCHAR));
        super.declareParameter(new SqlParameter(Singer.LAST_NAME, Types.VARCHAR));
        super.declareParameter(new SqlParameter(Singer.BIRTH_DATE, Types.DATE));
        super.setReturnGeneratedKeys(true);
    }
}
