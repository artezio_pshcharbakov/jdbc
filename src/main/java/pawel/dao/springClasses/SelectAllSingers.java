package pawel.dao.springClasses;

import org.springframework.jdbc.object.MappingSqlQuery;
import org.springframework.stereotype.Component;
import pawel.model.Singer;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class SelectAllSingers extends MappingSqlQuery<Singer> {

    public SelectAllSingers(DataSource ds) {
        super(ds, Singer.FIND_ALL_SINGERS_QUERY);
    }

    @Override
    protected Singer mapRow(ResultSet rs, int rowNum) throws SQLException {
        Singer singer = new Singer();
        singer.setGuid(rs.getString(Singer.GUID));
        singer.setFirstName(rs.getString(Singer.FIRST_NAME));
        singer.setLastName(rs.getString(Singer.LAST_NAME));
        singer.setBirthDate(rs.getDate(Singer.BIRTH_DATE));
        return singer;
    }
}
