package pawel.dao.springClasses;

import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.SqlUpdate;
import org.springframework.stereotype.Component;
import pawel.model.Singer;

import javax.sql.DataSource;
import java.sql.Types;

@Component
public class UpdateSinger extends SqlUpdate {

    private static final String UPDATE_SINGER_QUERY = "update singer set first_name=:first_name, " +
            "last_name=:last_name, birth_date=:birth_date where guid = :guid";

    public UpdateSinger(DataSource ds) {
        super(ds, UPDATE_SINGER_QUERY);
        super.declareParameter(new SqlParameter(Singer.FIRST_NAME, Types.VARCHAR));
        super.declareParameter(new SqlParameter(Singer.LAST_NAME, Types.VARCHAR));
        super.declareParameter(new SqlParameter(Singer.BIRTH_DATE, Types.DATE));
        super.declareParameter(new SqlParameter(Singer.GUID, Types.VARCHAR));
    }
}
