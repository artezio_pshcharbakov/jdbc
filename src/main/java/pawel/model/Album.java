package pawel.model;

import java.util.Date;

public class Album {

    private String guid;
    private String singerGuid;
    private String title;
    private Date releaseDate;

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getSingerGuid() {
        return singerGuid;
    }

    public void setSingerGuid(String singerGuid) {
        this.singerGuid = singerGuid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }
}
