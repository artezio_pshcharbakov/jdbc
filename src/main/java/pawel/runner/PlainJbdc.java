package pawel.runner;

import pawel.dao.PlainSingerDao;
import pawel.dao.SingerDao;
import pawel.model.Singer;

import java.sql.Date;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class PlainJbdc {

    private static SingerDao singerDao = new PlainSingerDao();

    public static void main(String[] args) {
        Singer singer = new Singer();
        singer.setFirstName("Nick");
        singer.setLastName("Jason");
        singer.setBirthDate(new Date(new GregorianCalendar(1991, Calendar.AUGUST, 15).getTime().getTime()));

        singerDao.insert(singer);//insert
        String randomRow = singerDao.findAll().get(0).getGuid();
        singer.setBirthDate(new Date(new GregorianCalendar(1980, Calendar.OCTOBER, 15).getTime().getTime()));
        singer.setFirstName("Carl");
        singer.setLastName("Gin");
        singerDao.update(singer);//update
        singerDao.delete(randomRow);//delete
        singerDao.findAll().forEach(System.out::println);//findAll
    }

}
