package pawel.runner;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import pawel.config.AppConfiguration;
import pawel.dao.SingerDao;
import pawel.model.Singer;

import java.sql.Date;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class SpringRunner {

    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(AppConfiguration.class);
        SingerDao springDaoClasses = context.getBean("springDaoClasses", SingerDao.class);

        springDaoClasses.findAll().forEach(System.out::println);//findAll

        Singer singer = new Singer();
        singer.setFirstName("Pavel");
        singer.setLastName("JUnit");
        singer.setBirthDate(new Date(new GregorianCalendar(1999, Calendar.DECEMBER, 25).getTime().getTime()));
        String insertKey = springDaoClasses.insert(singer);//insert
        System.out.println(insertKey);

        singer.setFirstName("Mockito");
        singer.setLastName("Tdd");
        springDaoClasses.update(singer);

    }
}
