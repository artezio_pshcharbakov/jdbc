package pawel.runner;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import pawel.config.AppConfiguration;
import pawel.dao.SingerDao;
import pawel.model.Singer;

import java.sql.Date;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class SpringTemplateRunner {
    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(AppConfiguration.class);
        SingerDao singerDao = context.getBean("jdbcTemplateDao", SingerDao.class);

        Singer singer = singerDao.findAll().get(0);//findAll
        singer.setFirstName("Harry");
        singer.setLastName("Kein");
        singerDao.update(singer);//update

        Singer mack = new Singer();
        mack.setFirstName("Mack");
        mack.setLastName("Jakson");
        mack.setBirthDate(new Date(new GregorianCalendar(1975, Calendar.MARCH, 12).getTime().getTime()));
        singerDao.insert(mack);//insert

        singerDao.delete(singerDao.findAll().get(0).getGuid());//delete
    }
}
